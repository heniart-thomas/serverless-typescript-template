# Lambda Node TypeScript Templacte

# Installation/deployment instructions

Follow the instructions below to deploy your project.

> **Requirements**:
> - NodeJS `lts/hydrogen (v.18.14.0)`. If you're using [nvm](https://github.com/nvm-sh/nvm),
    run `nvm install && nvm use` to ensure you're using the same Node version in local and in your lambda's runtime.

## Using Yarn

- Run `yarn` to install the project dependencies

# Test

Run `yarn test`

# Environment

- Run `cp .env.template .env` and fill variables

# Serverless

To test locally your function:

Run `yarn run helloWorld`

And output should look like:

```text
DOTENV: Loading environment variables from .env:
         - AWS_ACCESS_KEY_ID
         - AWS_SECRET_ACCESS_KEY
         - AWS_REGION
         - AWS_DEFAULT_REGION
{
    "statusCode": 200,
    "body": "{\"message\":\"Hello World !\"}"
}
✨  Done in 3.02s.
```

# Troubleshoot

## Known issue with enums

A problem exists between typescript, serverless and enums.

Instead of

```typescript
export enum MyEnum {
    Red = "red"
}

import {MyEnum} from "package"

const red = MyEnum.Red
```

Use

```typescript
export enum MyEnum {
    Red = "red"
}

import {MyEnum} from "package"

const red = 'red' as MyEnum.Red
```
