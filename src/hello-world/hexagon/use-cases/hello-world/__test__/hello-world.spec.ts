import {helloWorld} from "../hello-world";

describe("Hello World", () => {
    it("says hello world", async () => {
        expect(await helloWorld()).toEqual("Hello World !");
    })
})