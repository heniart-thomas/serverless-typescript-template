import * as process from "process";
import {config} from "dotenv";
import {helloWorld} from "../hexagon/use-cases/hello-world/hello-world";

export type Dependencies = {
    helloWorld: typeof helloWorld
};

export const buildDependencies = async (configuration): Promise<Dependencies> => {
    config({
        override: true,
    });

    switch (process.env.APP_ENV) {
        case "production":
            return buildProdDependencies(configuration);
        default:
            return buildDemoDependencies(configuration);
    }
};

const buildProdDependencies = async (configuration): Promise<Dependencies> => {
    return {
        helloWorld: helloWorld
    };
};

const buildDemoDependencies = async (configuration): Promise<Dependencies> => {
    return {
        helloWorld: helloWorld
    };
};
