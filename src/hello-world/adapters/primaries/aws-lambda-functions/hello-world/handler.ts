import {buildDependencies} from "../../../../config/dependencies";

export const main = async (event) => {
    const dependencies = await buildDependencies(event);
    return {
        statusCode: 200,
        body: JSON.stringify({message: await dependencies.helloWorld()}),
    };
};
